# Ansible Collection - devilops.windows

## Overview

This is an [Ansible Collection](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html) designed to track
roles, collections, playbooks, custom modules and plugins, created by the devilops developers at Duke Unversity.

## Refereneces

* Creating Ansible collections: https://docs.ansible.com/ansible/latest/dev_guide/developing_collections_creating.html
* Collection Structure: https://docs.ansible.com/ansible/latest/dev_guide/developing_collections_structure.html#collection-structure
* Developing collections: https://docs.ansible.com/ansible/latest/dev_guide/developing_collections.html#developing-collections

## Local Setup

1. Ensure ansible collection path includes parent directory of your collection directory: `export ANSIBLE_COLLECTIONS_PATH="$PWD/../:$ANSIBLE_COLLECTIONS_PATH"`

## Installation

todo

## Build from source

todo

## Testing

todo

## Contribute

todo